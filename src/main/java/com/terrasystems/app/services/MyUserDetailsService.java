package com.terrasystems.app.services;

import com.terrasystems.app.dao.User;
import com.terrasystems.app.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



/**
 * Created by Foz on 24.03.2016.
 */
@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    UserDao userDao;
    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();
    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        User client =  userDao.findByUsername(username);
        detailsChecker.check(client);
        return client;
    }
}
