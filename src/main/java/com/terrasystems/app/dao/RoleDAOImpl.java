package com.terrasystems.app.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

/**
 * Created by Foz on 25.03.2016.
 */
@Repository
@Transactional
public class RoleDAOImpl implements RoleDao<Role, Long> {
    @Autowired
    EntityManagerFactory entityManagerFactory;
    @Autowired
    SessionFactory sessionFactory;


    @Override
    public Role create(Role role) {
        Session session = sessionFactory.getCurrentSession();
        session.save(role);
        return role;
    }

    @Override
    public Role read(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return (Role) session.get(Role.class, id);
    }

    @Override
    public Role update(Role role) {
        Session session = sessionFactory.getCurrentSession();
        session.update(role);
        return role;
    }

    @Override
    public void delete(Role role) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(role);
    }

}
