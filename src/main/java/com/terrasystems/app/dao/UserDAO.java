package com.terrasystems.app.dao;


import java.util.List;

/**
 * Created by Foz on 24.03.2016.
 */

public interface UserDao  {
    User create(User user);
    User read(Long id);
    User update(User user);
    void delete(User user);
    User findByUsername(String username);
    List<User> getAll();

}
