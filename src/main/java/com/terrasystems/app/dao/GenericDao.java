package com.terrasystems.app.dao;

/**
 * Created by Foz on 28.03.2016.
 */
public interface GenericDao<T, PK> {
    T create(T t);
    T read(PK id);
    T update(T t);
    void delete(T t);
}
