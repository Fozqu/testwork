package com.terrasystems.app.dao;

/**
 * Created by Foz on 25.03.2016.
 */
public interface RoleDao<Role, Long> extends GenericDao<Role, Long> {

}
