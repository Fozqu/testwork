package com.terrasystems.app.controllers;

import com.terrasystems.app.dao.Role;

import com.terrasystems.app.dao.RoleDao;
import com.terrasystems.app.dao.User;

import com.terrasystems.app.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Foz on 24.03.2016.
 */
@RestController
public class UserController {
    @Autowired
    UserDao userDao;
    @Autowired
    RoleDao roleDao;

    @RequestMapping(value = "/api/users/create/{role}",method = RequestMethod.POST)
    @ResponseBody
    public String create(@RequestBody User user, @PathVariable String role) {
        User loadedUser = user;
        Role newrole = new Role(role);
        newrole.setUser(loadedUser);
        try {
            userDao.create(loadedUser);
            roleDao.create(newrole);
        }
        catch (Exception ex) {
            return "Error creating the student: " + ex.toString();
        }
        return "Student succesfully created! (id = " + user.getUserId() + ")";
    }

    @RequestMapping(value = "/api/user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User findUserById(@PathVariable String id) {
        User user =  userDao.read(Long.parseLong(id));
        return user;
    }

    @RequestMapping(value = "/api/users/{name}", method = RequestMethod.GET)
    @ResponseBody
    public User findUserByName(@PathVariable String name) {
        User user =  userDao.findByUsername(name);
        return user;
    }

    @RequestMapping(value = "/api/users", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getAll() {
        return userDao.getAll();
    }

    @RequestMapping(value = "/api/users/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String deleteById(@PathVariable String id) {
        User user =  userDao.read(Long.parseLong(id));
        try {
            userDao.delete(user);
        }
        catch (Exception e){
            return "Unable to delete";
        }
        return "Deleted";
    }
}
