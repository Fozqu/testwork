package com.terrasystems.app.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Foz on 25.03.2016.
 */
@RestController
public class AuthController {
    @RequestMapping(value = "/api/test", method = RequestMethod.GET)
    @ResponseBody
    public String test() {
        return "Hello from Auth";
    }

    @RequestMapping(value = "/admin/test", method = RequestMethod.GET)
    @ResponseBody
    public String testAdmin() {
        return "Hello from Auth";
    }
}
