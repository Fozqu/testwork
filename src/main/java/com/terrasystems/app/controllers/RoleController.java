package com.terrasystems.app.controllers;

import com.terrasystems.app.dao.Role;
import com.terrasystems.app.dao.RoleDao;
import com.terrasystems.app.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Foz on 25.03.2016.
 */
@RestController
public class RoleController {
    @Autowired
    UserDao userDao;
    @Autowired
    RoleDao roleDao;

    @RequestMapping(value = "/api/role/create/{role}", method = RequestMethod.GET)
    @ResponseBody
    public String create(@PathVariable String role) {

        roleDao.create(new Role(role));
        return "Created";
    }
}
